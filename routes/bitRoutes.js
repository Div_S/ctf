const authen=require('../middleware/authen')
const express = require('express')
const router = express.Router()
const controller = require('../controllers/bidController')

router.post('/bid',authen,controller.bid)

router.get('/currentbid',authen,controller.currentBid)

router.get('/history',authen,controller.history)

router.get('/soldproducts',authen,controller.soldproducts)

module.exports=router
