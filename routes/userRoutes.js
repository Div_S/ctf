const express=require("express")
const userController=require('../controllers/userController')
const passport=require('../config/passport')
const validator=require('../middleware/validators')
const authen=require('../middleware/authen')
const router=express.Router();
const verifytoken=require('../middleware/checkToken')

router.post('/signup',validator.validateSignup(),validator.validate,userController.signup)

router.post('/login',validator.validateLogin(),validator.validate,userController.login)

router.post('/loginconfirmation/',verifytoken.checkToken, passport.authenticate("user-login"),(req,res)=>{
    res.status(200).json({
        "auth":req.sessionID
    })
}) 


router.post('/resetpassword',userController.sendToken)

router.post('/passwordreset/',userController.verifyToken)
   
router.get('/confirmation/',userController.confirmation)

router.get('/auth/google', passport.authenticate("google", {
    scope: ['profile', 'email'],
}))

router.get('/auth/google/callback', passport.authenticate("google"), (req, res) => {
    res.status(200).json({
        "auth": req.sessionID
    })
})

router.get('/logout',(req,res)=>{
    req.logOut();
    console.log(req.user)
    console.log(req.session)
   // console.log(req.user)
   res.status(200).json({
       "message":"Logged out"
   })

})
router.get('/user',authen,(req,res)=>{
    res.send("Hello")
})
module.exports=router