const express = require('express')
const router = express.Router()
const controller = require('../controllers/adminController')
const passport=require('../config/passport')
const validator=require('../middleware/validators')
const authen=require('../middleware/adminvalidate')

router.post('/register',controller.adminsignup)

router.post('/login', validator.validateLogin(), validator.validate, passport.authenticate("admin-login"), (req, res) => {
    res.status(200).json({
        "auth": req.sessionID
    })
})


router.get('/products',authen, controller.getProductList)

router.route('/product/:id')
    .get(authen,controller.getProduct)
    .post(authen,controller.createProduct)
    .put(authen,controller.updateProduct)
    .delete(authen,controller.deleteProduct)

router.post('/sellproduct',authen,controller.sell)

module.exports = router