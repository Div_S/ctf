const error=(res,err)=>{
    console.log(err)
    let { statusCode,message  }=err
    message= message || "Unexpected Error"
    statusCode =statusCode || 500
    res.status(statusCode).send({
        status:"Error",
        message,
        statusCode,
    })


}

const response=(res,data)=>{ res.status(200).json(data)}

module.exports={ error,response}