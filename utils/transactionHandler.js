const connection = require('../config/connection');
const responseHandler  = require('./responseHandler')
const { promisify } = require('util')

module.exports = {
    beginTransaction: promisify(function (callback) {
        connection.beginTransaction(function (err) {
            callback(err);
        });
    }),
    rollback: promisify(function (res, err) {
        connection.rollback(function (rollErr) {
            if (rollErr) {
                responseHandler.error(res, rollErr);
            }
            else {
                responseHandler.error(res, err);
            }
        });
    }),
    commit: promisify(function (res, data) {
        connection.commit(function (commitErr) {
            if (commitErr) {
                connection.rollback(function (rollErr) {
                    if (rollErr) {
                        responseHandler.error(res, rollErr);
                    }
                    else {
                        responseHandler.error(res, commitErr);
                    }
                });
            }
            else {
                responseHandler.response(res, data);
            }
        })
    })
}