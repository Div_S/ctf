const bcrypt = require('bcrypt')

module.exports.generateHash = async (password) => await bcrypt.hash(password,8)

module.exports.verifyPassword = async (password,hash) => await bcrypt.compare(password,hash)

