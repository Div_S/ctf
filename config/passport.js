const passport = require('passport');
const CustomStrategy = require('passport-custom').Strategy;
const LocalStrategy = require('passport-local').Strategy
const User = require('../dao/user')
const express = require("express")
const util = require('../utils/authUtils')
const app = express();
const GoogleStrategy = require('passport-google-oauth2').Strategy;
let jwt = require('jsonwebtoken')
const config=require('./config.json')



passport.use(new GoogleStrategy({
  clientID: config.google.clientID,
  clientSecret: config.google.clientSecret,
  callbackURL: config.google.callbackURL,
  passReqToCallback: true
},
  function (request, accessToken, refreshToken, profile, cb) {

    let user = profile._json
    console.log(user)
    User.getUserByEmail(user.email)
      .then(data => {
        if (data.length >= 1)
          cb(null, data[0])
        else {

          let userinfo = {
            fname: user.given_name,
            lname: user.family_name,
            email: user.email,
            verified: user.email_verified
          }
          User.createUser(userinfo)
            .then((data) => cb(null, data[0]))
            .catch((err) => cb(null, false))

        }

      })
      .catch(err => cb(err))
  }
));


passport.serializeUser((user, cb) => {
  cb(null, user.email);
});

passport.deserializeUser((email, cb) => {
  User.getUserByEmail(email)
    .then(user => {
      if (user.length > 0)
        cb(null, user[0])
      else
        cb(null, false)
    })
    .catch(err => cb(err))
});


passport.use("user-login", new CustomStrategy((req, cb) => {
  if (!req.tokenData) return cb("Invalid request")
  User.getUserByEmail(req.tokenData.email)
    .then(async data => {
      if (data.length > 0) {
        if(req.tokenData.otp === req.body.otp) {
          cb(null,data[0])
        } else {
          cb("Invalid otp")
        }
      }
      else
        cb("User does not exists")
    })
    .catch(err => cb(err))
}))

passport.use("admin-login", new LocalStrategy({ usernameField: 'email', passwordField: 'password' }, (username, password, cb) => {
  User.getUserByEmail(username)
      .then( async data => {
          if (data.length > 0 && data[0].isadmin) {
              const isAuth = await util.verifyPassword(password, data[0].password);
              if(isAuth)
                  cb(null,data[0])
              else
                  cb("Invalid Password")
          }
          else
              cb("User does not exists")
      })
      .catch(err => cb(err))
}))




module.exports = passport;