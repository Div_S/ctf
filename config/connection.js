const sql=require('mysql')
const { promisify } = require("util")
const config=require('./config.json')
const connection=sql.createConnection({
    host:config.mysql.host,
    port:config.mysql.port,
    user:config.mysql.user,
    password:config.mysql.password,
    database:config.mysql.database
})

connection.connect(function(err){
    if(!err)
    console.log("Database connected")
    else
    console.log(err)

})

connection.query=promisify(connection.query)
module.exports=connection