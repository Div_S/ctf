const express=require('express')
const bodyParser=require('body-parser')
const app = express()
const userrouter=require('./routes/userRoutes')
const adminrouter=require('./routes/adminRoutes')
const bidrouter=require('./routes/bitRoutes')
const session=require('express-session')
const passport=require('passport')
const cookieParser = require('cookie-parser'); 

//setup express app  

  


app.use(bodyParser.json());
app.use(cookieParser()); 

app.use(session({
    name: "auth",
    secret: 'sgfdhUHbbbfbKJmnncb',
    resave: false,
    saveUninitialized: true,
   // cookie: { secure: true }
}))

// app.use((req,res,next)=>{
//     let cookiearray=req.headers.cookie.split(";")
//     cookies={}
//     cookiearray.map()
// })
app.use(passport.initialize());
app.use(passport.session());

app.use('/user',userrouter)

app.use('/admin',adminrouter)

app.use('/',bidrouter)

app.get('/',(req,res)=>{
    console.log(req.cookies)
    
    res.send(req.cookie)
})



app.listen(process.env.PORT||3000,()=>{
    console.log("Server is listening")
})