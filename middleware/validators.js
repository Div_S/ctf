const { check,validationResult } = require('express-validator');

module.exports.validateSignup = () => {
    return [
    check('email').isEmail(),
    check('fname', "First name sholud be of minimiun length and should contain only alphabets").escape().trim().isAlpha(),
    check('phone', "Phone number sholud contain exactly 10 numbers").escape().trim().isLength({ max: 10, min: 10 }),
    check('password',"Password should be between 5 to 30 characters").trim().isLength({ min: 5, max: 30 }),
    check('password2',"Password and confirm password mismatches").custom((value,{req})=>{
        if(value == req.body.password)
            return true
        return false
    })]
}

module.exports.validateLogin = () => {
    return [
        check('email').isEmail(),
        check('password', "Password should be between 5 to 30 characters").trim().isLength({ min: 5, max: 30 })
    ]
}

module.exports.validate = (req, res, next) => {
    const errors = validationResult(req)
    if (errors.isEmpty()) {
        return next()
    }
    const extractedErrors = []
    errors.array().map(err => extractedErrors.push({ [err.param]: err.msg }))

    return res.status(422).json({
        errors: extractedErrors,
    })
}
