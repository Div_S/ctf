module.exports = (req,res,next) => {
    console.log(req.user)
    console.log(req.session)
    console.log(req.isAuthenticated())

    if(req.isAuthenticated() && req.session && req.user ) {
        if (req.user.verified)
            return next()
        return res.status(401).json({ message: "Please verify your email" })
    }
    return res.status(401).json({message:"Unauthorized"})
}


