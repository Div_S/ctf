let jwt = require('jsonwebtoken');
const config = require('../config/config.json');
const cookieParser = require('cookie-parser'); 
//setup express app  

const express=require('express')
const app=express();
app.use(cookieParser()); 



let checkToken = (req, res, next) => {
   console.log(req.headers.cookie)
  // let s=req.headers.cookie
  // c=s.split(';')
  // console.log(c[0].substr(7,c[0].length))
   console.log(req.session.cookie)
  // console.log(req.body)
  console.log(req.cookies)
  let token=req.cookies.signin
  if (token) {
    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      } else {
        req.tokenData = {
          email :decoded.email,
          otp :decoded.otp
        };
        console.log(req.tokenData)
        next();
      }
    });
  } else {
    return res.json({
      success: false,
      message: 'Auth token is not supplied'
    });
  }
};

module.exports = {
  checkToken: checkToken
}

