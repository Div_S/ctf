const productDao = require('../dao/product')
const bidDao = require('../dao/bid')
const responseHandler = require('../utils/responseHandler')

module.exports.bid = (req, res) => {
    let bidprice = req.body.price;
    let userid = req.user.id;
    let productid = req.query.id;
    productDao.getProductById(productid)
        .then(data => {
        
            console.log(data[0])
            if (data[0].sold || data[0].initalprice >= bidprice) {
                responseHandler.response(res, { "message": "Cannot Bid product" })
            }
            else {
                let bid = {
                    productid: productid,
                    userid: userid,
                    bidprice: bidprice
                }
                console.log(bid)
                bidDao.bid(bid)
                    .then(data =>
                        responseHandler.response(res, { "message": "Successfully bid the product" }))
                    .catch(err => responseHandler.error(res, err))
            }
        })

}

module.exports.soldproducts=(req,res)=>{
    productDao.getSoldProducts()
    .then(data=>responseHandler.response(res,data[0]))
    .catch(err=>responseHandler.error(res,err))
}
module.exports.currentBid=(req,res)=>
{
    bidDao.currentBid()
    .then(data=>responseHandler.response(res,data))
    .catch(err=>responseHandler.error(res,err))
}
module.exports.history=(req,res)=>
{
    bidDao.historyBid(req.query.id)
    .then(data=>responseHandler.response(res,data))
    .catch(err=>responseHandler.error(res,err))
}