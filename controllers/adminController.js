const productDao = require('../dao/product')
const userDao=require('../dao/user')
const bidDao=require('../dao/bid')
const responseHandler = require('../utils/responseHandler')
const utils=require('../utils/authUtils')

module.exports.adminsignup = async (req, res) => {
    req.body.password = await utils.generateHash(req.body.password)
    delete req.body.password2

    userDao.getUserByEmail(req.body.email)
        .then(data => {
            if (data.length > 0)
                responseHandler.response(res, { "message": "Admin already exists" })
            else {
                userDao.createUser(req.body)
                    .then((data) => {
                        responseHandler.response(res,{message:"Admin created"})
                    })
                    .catch(err => responseHandler.error(res, err))
            }
        })
        .catch(err => responseHandler.error(res, err))
}



module.exports.createProduct = (req, res) => {
    let product = req.body.product
    if (product) {
        productDao.createProduct(product)
            .then(data => responseHandler.response(res, data))
            .catch(err => responseHandler.error(res, err))
    } else {
        responseHandler.error(res, { statusCode: 400, "message": "Invalid body" })
    }
}


module.exports.getProduct = (req, res) => {
    if (req.params.id) {
        productDao.getProductById(req.params.id)
            .then(data => responseHandler.response(res, data))
            .catch(err => responseHandler.error(res, err))
    } else {
        responseHandler.error(res, { statusCode: 400, "message": "Invalid body" })
    }
}

module.exports.getProductList = (req, res) => {
    productDao.getAllProducts()
        .then(data => responseHandler.response(res, data))
        .catch(err => responseHandler.error(res, err))
}

module.exports.updateProduct = (req, res) => {
    let product = req.body.product
    if (req.params.id && product) {
        productDao.updateProduct(product, req.params.id)
            .then(data => responseHandler.response(res, { id: req.params.id }))
            .catch(err => responseHandler.error(res, err))
    } else {
        responseHandler.error(res, { statusCode: 400, "message": "Invalid body" })
    }
}

module.exports.deleteProduct = (req, res) => {
    if (req.params.id) {
        productDao.deleteProduct(req.params.id)
            .then(data => responseHandler.response(res, { id: req.params.id }))
            .catch(err => responseHandler.error(res, err))
    } else {
        responseHandler.error(res, { statusCode: 400, "message": "Invalid body" })
    }
}

module.exports.sell = (req, res) => {
    if (req.query.id) {
        productDao.getProductById(req.query.id)
            .then(data => {
                if (data[0].sold == 1) {
                    responseHandler.error(res, { "message": "Product already sold" })

                }
                else {
                    bidDao.maxPrice(req.query.id)
                    .then(data=>{
                        console.log(data[0].bidprice)
                        console.log(data[0].userid)
                    productDao.sellProduct(data[0].bidprice,data[0].userid, req.query.id)
                        .then(result => {
                           // if (result.length > 0)
                                responseHandler.response(res, { "message": "Product sold" })
                        })
                        .catch(err => responseHandler.error(res, err))
                    })
                    .catch(err=>responseHandler.error(res,err))
                }
            })
            .catch(err=>responseHandler.error(res,err))
    }
}