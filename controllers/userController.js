const userDao = require('../dao/user')
const utils = require('../utils/authUtils')
const responseHandler = require('../utils/responseHandler')
const nodemailer = require('nodemailer')
const crypto = require('crypto')
const transactionHandler = require('../utils/transactionHandler')
const jwt = require('jsonwebtoken')
const config = require('../config/config.json')
const sendMail = require('../utils/mailUtil').sendMail;


module.exports.login = async (req, res) => {
    userDao.getUserByEmail(req.body.email)
        .then(data => {
            if (data.length > 0) {
                const isAuth = utils.verifyPassword(req.body.password, data[0].password);
                if (isAuth) {
                    console.log(data[0])
                    let ontimepass=Math.floor(100000 + Math.random() * 900000)
                    let otp=ontimepass.toString()
                    console.log(otp)
                    let token = jwt.sign({ email: req.body.email, otp },
                        config.secret,
                        {
                            expiresIn: 10 * 60 * 1000,
                        }
                    );
                    sendMail(req.body.email, otp)
                        .then(() => {
                            res.cookie("signin", token, { maxAge: 10 * 60 * 1000, httpOnly: true })
                            console.log(req.cookies)
                            responseHandler.response(res, { message: "Otp sent successfully" })
                        })
                        .catch(err => responseHandler.error(res, err))
                }
                else
                    responseHandler.error(res, { "message": "Invalid password" })
            }
            else {
                responseHandler.error(res, { "message": "User not found" })
            }
        })
        .catch(err=>responseHandler.error(res,err))
}



module.exports.signup = async (req, res) => {
    req.body.password = await utils.generateHash(req.body.password)
    delete req.body.password2

    userDao.getUserByEmail(req.body.email)
        .then(data => {
            if (data.length >=1 )
                responseHandler.response(res, { "message": "User already exists" })
            else {
                userDao.createUser(req.body)
                    .then((data) => {
                        var token = crypto.randomBytes(16).toString('hex');
                        const expires = (Date.now() / 1000) + (10 * 60)
                        console.log(token)
                        userDao.createVerifyToken({ email: req.body.email, expires, token })
                            .then(data => {
                                let text="https://ctf-task.herokuapp.com/user/confirmation/?email=" + req.body.email + '&token=' + token
                                   sendMail(req.body.email,text)
                                   .then(data=>responseHandler.response(res,{"message":"Verification mail sent"}))
                                   .catch(err=>responseHandler.error(res,err))
                            }
                            )
                            .catch((err) => responseHandler.error(res, err))
                    })
                    .catch(err => responseHandler.error(res, err))
            }
        })
        .catch(err => responseHandler.error(res, err))
}





module.exports.confirmation = async (req, res) => {
    let mail = req.query.email
    let token = req.query.token
    userDao.getVerifyToken(mail)
        .then(data => {
            console.log(data[0].token)
            if (data[0].token == token && data.length > 0) {
                console.log("true")
                userDao.getUserByEmail(mail)
                    .then(data => {
                        if (data[0].verified)
                            return responseHandler.response(res, { "message": "User already verified" })
                        else {
                            userDao.verified(mail)
                                .then(user => {
                                    return responseHandler.response(res, { "message": "Verification successful" })

                                })
                                .catch(err => responseHandler.error(res, err))


                        }
                    })
                    .catch(err => responseHandler.error(res, err))

            }
            else {
                responseHandler.error(res, { "message": "Invalid token or token expired" })
            }
        })
        .catch(err => responseHandler.error(res, err))

}

module.exports.sendToken = async (req, res) => {
    if (req.body.email) {
        try {
            const data = await userDao.getUserByEmail(req.body.email)
           // console.log(req.query)
            if (data.length > 0) {
                if (data[0].verified) {
                    const data = await userDao.getResetToken(req.body.email)
                    const token = crypto.randomBytes(30).toString("hex")
                    const expires = (Date.now() / 1000) + (10 * 60)
                    if (data.length > 0) {
                        await userDao.updateResetToken({ email: req.body.email, expires, token })
                        let text= "https://ctf-task.herokuapp.com/user/passwordreset/?email=" + req.body.email + '&code=' + token

                        sendMail(req.body.email,text)
                        .then(data=>responseHandler.response(res,{message:"Mail sent"}))
                        .catch(err=>responseHandler.error(res,err))
                        // responseHandler.response(res, { message: "Token sent to email successfully" })
                    } else {
                        await userDao.createResetToken({ email: req.body.email, expires, token })

                        let text= "https://ctf-task.herokuapp.com/user/passwordreset/?email=" + req.body.email + '&code=' + token

                        sendMail(req.body.email,text)
                        .then(data=>responseHandler.response(res,{message:"Mail sent"}))
                        .catch(err=>responseHandler.error(res,err))
                        // responseHandler.response(res, { message: "Token sent to email successfully" })
                    }
                } else {
                    responseHandler.error(res, { statusCode: 401, "message": "User doesnot exists" })
                }
            } else {
                responseHandler.error(res, { statusCode: 401, "message": "User doesnot exists" })
            }
        } catch (err) {
            responseHandler.error(res, err)
        }
    } else {
        responseHandler.error(res, { statusCode: 400, "message": "Invalid body" })
    }
}

module.exports.verifyToken = async (req, res) => {
    if (req.query.email && req.query.code) {
        const data = await userDao.getResetToken(req.query.email)
        console.log(data[0])
        if (data.length > 0 && data[0].token == req.query.code && req.body.password == req.body.password2) {
            transactionHandler.beginTransaction()
                .then(
                    async () => {
                        try {
                            const password = await utils.generateHash(req.body.password)
                            await userDao.deleteResetToken(req.query.email)
                            await userDao.updatePassword(req.body.password, req.query.email)
                            await transactionHandler.commit(res, {})
                        } catch (err) {
                            await transactionHandler.rollback(res, err)
                        }
                    }
                )
        } else {
            responseHandler.error(res, { statusCode: 400, "message": "Invalid request" })
        }
    } else {
        responseHandler.error(res, { statusCode: 400, "message": "Invalid request" })
    }
}
/* module.exports.reset = async (req, res) => {

    userDao.getUserByEmail(req.body.email)
        .then(data => {
            if (data.length > 0) {
                var token = crypto.randomBytes(16).toString('hex');
                const expires = (Date.now() / 1000) + (10 * 60)
                console.log(token)
                userDao.getResetToken(req.body.email)
                    .then(data => {
                        if (data.length > 0) {
                            userDao.createResetToken({ email: req.body.email, expires, token })
                                .then(data => {


                                    var mailOptions = {
                                        from: 'divyasaravanan0327@gmail.com',
                                        to: req.body.email,
                                        subject: 'Account Verification Token',

                                        text: "http://127.0.0.1:3000/passwordreset/" + req.body.email + '\/' + token
                                    };
                                    var transporter = nodemailer.createTransport(mailDetails);
                                    transporter.sendMail(mailOptions, function (err, data) {
                                        if (err) {
                                            console.log(err);
                                            return res.status(500).send({ msg: err.message });
                                        }
                                        else {
                                            console.log(data);
                                            res.status(200).send('A verification email has been sent to ' + req.body.email + '.');
                                        }
                                    });
                                })
                                .catch(err => responseHandler.error(res, err))
                        }
                        else {

                            userDao.updateResetToken({ email: req.body.email, expires, token })
                                .then(data => {


                                    var mailOptions = {
                                        from: 'divyasaravanan0327@gmail.com',
                                        to: req.body.email,
                                        subject: 'Account Verification Token',

                                        text: "http://127.0.0.1:3000/passwordreset/" + req.body.email + '\/' + token
                                    };
                                    var transporter = nodemailer.createTransport(mailDetails);
                                    transporter.sendMail(mailOptions, function (err, data) {
                                        if (err) {
                                            console.log(err);
                                            return res.status(500).send({ msg: err.message });
                                        }
                                        else {
                                            console.log(data);
                                            res.status(200).send('A verification email has been sent to ' + req.body.email + '.');
                                        }
                                    });
                                })
                                .catch(err => responseHandler.error(res, err))
                        }
                    })
                    .catch(err => responseHandler.error(res, err))
            }
        })
        .catch(err => responseHandler.error(res, err))
}

module.exports.passwordreset = async (req, res) => {
    let mail = req.params.email
    let token = req.params.token
    userDao.getResetToken(mail)
        .then(data => {
            console.log(data[0].token)
            if (data[0].token == token && data.length > 0) {
                console.log("true")
                let password = utils.generateHash(req.body.password)
                userDao.updatePassword(password, mail)
                    .then(data => {
                        userDao.deleteResetToken(mail)
                            .then(data => responseHandler.response(res, { "message": "Password change successful" }))
                            .catch(err => responseHandler.error(res, err))


                    }
                    )
            }
            else {
                responseHandler.response(res, { "message": "Invalid token or token expired" })
            }
        })
        .catch(err => responseHandler.error(res, err))

}

 */