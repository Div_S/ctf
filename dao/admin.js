const connection=require('../config/connection')

const createadmin= user => connection.query("insert into admin SET ?",[user])

const getAdminByEmail=email => connection.query('select * from admin where email =?',[email])

const getAdminById=id => connection.query('select * from admin where id =?',[id])


module.exports={createadmin,getAdminByEmail,getAdminById}