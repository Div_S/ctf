const connection = require('../config/connection')

const bid = (bid) => connection.query("INSERT INTO bid set ?", bid)

const historyBid=(pid)=>connection.query("SELECT * FROM (SELECT * from bid WHERE productid=?)as t ORDER BY t.id DESC LIMIT 5",[pid])

const currentBid=()=>connection.query("SELECT * from bid ")

const maxPrice=(pid)=>connection.query("SELECT * FROM (SELECT * from bid WHERE productid=?)as t ORDER BY t.id DESC LIMIT 5",[pid])

module.exports={bid,historyBid,currentBid,maxPrice}