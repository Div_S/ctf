const connection=require('../config/connection')

const createUser= user => connection.query("insert into users SET ?",[user])

const getUserByEmail=email => connection.query('select * from users where email =?',[email])

const getUserById=id => connection.query('select * from users where id =?',[id])

const verified = email=> connection.query("update users set verified = '1' where email = ?",[email]) 
const updatePassword = (password,email) =>
    connection.query("update users set password = ? where email = ?",[password,email])

    const createVerifyToken = payload =>
    connection.query("INSERT INTO verification set ?", payload)

const getVerifyToken = email =>
    connection.query("select * from verification where email = ?", [email])

const deleteVerifyToken = email =>
    connection.query("DELETE FROM verification WHERE email = ?", [email])

const updateVerifyToken = payload =>
    connection.query("update verification set token  = ?, expires = ? where email = ?", [payload.token,payload.expires,payload.email])

const createResetToken = payload =>
    connection.query("INSERT INTO resets set ?", payload)

const getResetToken = email =>
    connection.query("select * from resets where email = ?", [email])

const deleteResetToken = email =>
    connection.query("DELETE FROM resets WHERE email = ?", [email])

const updateResetToken = payload =>
    connection.query("update resets set token  = ?, expires = ? where email = ?", [payload.token,payload.expires,payload.email])

module.exports = {
    getUserByEmail,
    getUserById,
    createUser,
    createResetToken,
    getResetToken,
    updateResetToken,
    deleteResetToken,
    updatePassword,
    createVerifyToken,
    deleteVerifyToken,
    updateVerifyToken,
    getVerifyToken,
    verified
}

