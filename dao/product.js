const connection = require('../config/connection')

const createProduct = (product) => connection.query("INSERT INTO products set ?", product)

const getProductById = (id) => connection.query("select * from products where id = ?", [id])

const getSoldProducts=() => connection.query("select * from products where sold ='1'")

const getAllProducts = () => connection.query("select * from products")

const updateProduct = (product) => connection.query("update products set ? where id = ?", [product, product.id])

const sellProduct = (price,soldto, id) => connection.query("update products set soldprice = ? ,soldto=?, sold = true where id = ?", [price,soldto,id])

const deleteProduct = (id) => connection.query("DELETE FROM products WHERE id = ?", [id])


module.exports = {
    createProduct,
    getProductById,
    getAllProducts,
    updateProduct,
    sellProduct,
    getSoldProducts,
    deleteProduct
}